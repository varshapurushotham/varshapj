#include<stdio.h>
int input()
{
    int num;
    printf("Enter the value of number:\n");
    scanf("%d", &num);
}
int compute(int n1,int n2, int n3)
{
    int result;
    if(n1>n2&&n1>n3)
    {
        result=n1;
    }
    else if(n2>n3)
    {
        result=n2;
    }
    else
    {
        result=n3;
    }
    return result;
}
void output(int result)
{
    printf("Largest of the 3 numbers=%d", result);
}
int main()
{
    int num1,num2,num3,r;
    num1=input();
    num2=input();
    num3=input();
    r=compute(num1,num2,num3);
    output(r);
    return 0;
    
}
